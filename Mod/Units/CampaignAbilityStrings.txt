[ANha]
Name=Harvest
Tip=|cffffcc00G|rather
Ubertip="Mines gold from gold mines and harvests lumber from trees."
Untip=R|cffffcc00e|rturn Resources
Unubertip="Return the carried resources to the nearest Temple of the Tides."
Hotkey=G
Unhotkey=E
[ANpa]
Name=Parasite
Tip=Pa|cffffcc00r|rasite
Ubertip="Afflicts a target enemy unit with a deadly parasite that deals <ANpa,DataA1> damage per second for <ANpa,Dur1> seconds. If the afflicted unit dies while under the effect of Parasite, a minor minion will spawn from its corpse."
Untip="|cffc3dbffRight-click to activate auto-casting.|r"
Unubertip="|cffc3dbffRight-click to deactivate auto-casting.|r"
Hotkey=R
Unhotkey=R
[BNpa]
Bufftip="Parasite"
Buffubertip="This unit has been inflicted with a parasite; it will take damage over time, and if it dies while still afflicted, a minion will spawn from its corpse."
[ANcl]
Name=Channel
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[IBA8]
Name=Channel
Tip="Rune of Repair"
Ubertip="Repairs buildings near the worker. Stacks with other Runes and Coral Statues."
Hotkey=
[IBA5]
Name=Channel
Tip="Scroll of Stone"
Ubertip="When used, the scroll heals 400 hitpoints, 200 mana and gives 8 bonus armor for 30 seconds to all allied units in a huge area."
Hotkey=
[IBA4]
Name=Channel
Tip="Cheese"
Ubertip="Allowing you to build one more legendary structure."
Hotkey=
[IBA7]
Name=Channel
Tip="Blast Staff"
Ubertip="Launches one magic projectile per second at a random nearby enemy. Projectiles deal 60 spell damage."
Hotkey=
[IBA6]
Name=Channel
Tip="Drum'n'Bass Bassline Generator"
Ubertip="Increases the movementspeed of all friendly units by 15% and their attackspeed by 30%."
Hotkey=
[IBA1]
Name=Channel
Tip="Quad Damage"
Ubertip="Increases the damage of all allied units by 300% for 30 seconds."
Hotkey=
[IBA0]
Name=Channel
Tip="Double Damage"
Ubertip="Increases the damage of all allied units by 100% for 30 seconds."
Hotkey=
[IBA3]
Name=Channel
Tip="Drum'n'Bass Bass Drums"
Ubertip="Gives all allied units +33% damage."
Hotkey=
[IBA2]
Name=Channel
Tip="Orb of Lightning"
Ubertip="Allows your worker to fire a chainlightning at target enemy unit once every minute. The chainlightning hits 10 targets, dealing 1000 damage to the first target."
Hotkey=
[A0DZ]
Name=Channel
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A0DX]
Name=Channel
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A0DW]
Name=Channel
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A0EK]
Name=Channel
Tip="Mana Transfusion"
Ubertip="Transfers 0.30 mana per minute to the targeted building. Targeted building can not be changed to another one. For nonelemental buildings mana regeneration is twice slow."
Hotkey=Q
[A0ER]
Name=Channel
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A0EO]
Name=Channel
Tip="Cancel"
Ubertip="Cancels all ongoing recruitment processes."
Hotkey=C
[A0CV]
Name="f_ranged"
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A0DD]
Name=Channel
Tip="|cffFFFF00Choose Chaos Legion|r"
Ubertip="The Forces of Chaos summon horrible demons and use destructive magic to defeat their enemies. Chaos has no siege unit, but many strong units with chaos damage. Chaos has NO TOWER!"
Hotkey=C
[A0DI]
Name=Channel
Tip="|cffFFD900Choose Naga Faction|r"
Ubertip="The Nagas have above average powerful units with a high hitpoint regeneration. In addition many expensive special buildings are available to them. But note that this race has NO TOWERS!"
Hotkey=C
[A0DJ]
Name=Channel
Tip="|cffFFD900Choose Northern Realms|r"
Ubertip="The nordic creatures stand united against everyone who wants to invade their beloved icy plains and mountains. They use strong freezing and chilling magic. Nordics can defend their buildings with a chilling splash tower."
Hotkey=C
[A0DK]
Name=Channel
Tip="|cffFFD900Choose Night Elves|r"
Ubertip="Night Elfs have split from the Elven Union and fight on their own for their goddess Elune. They are fragile creatures but they know how to evade enemies blows and protect themselves from harmful magic. They have a tower with feedback."
Hotkey=C
[A0DL]
Name=Channel
Tip="|cffFFD900Choose the Undead Forces|r"
Ubertip="Undead Forces use necromantic magic to reanimate dead units. They bring death and disease to their enemies. An undead builder is able to construct a spirit tower to defend your base."
Hotkey=C
[A0DE]
Name=Channel
Tip="|cffFFD900Choose the Elven Union|r"
Ubertip="The elven races have gathered to defend their realms. Elves use strong magic to weaken enemies and aid friendly units. They've got a pretty good siege weapon and a low ranged tower with multishot."
Hotkey=C
[A0DF]
Name=Channel
Tip="|cffFFD900Choose Corrupted Ones Conglomerate|r"
Ubertip="Corrupted by evil demons, these former friendly creatures have turned into minions of darkness. They use dark magic to influence their enemies and can build a tower with splash damage."
Hotkey=C
[A0DG]
Name=Channel
Tip="|cffFFD900Choose Human Alliance|r"
Ubertip="Humans use many cheap units to overrun the enemy. They can use artillery to demolish the enemy base without having to attack it directly. This race has a basic defense tower."
Hotkey=C
[A0DH]
Name=Channel
Tip="|cffFFD900Choose the Orcish Horde|r"
Ubertip="The Orcish Horde trains tough fighters and supports them with shamanic magic. Their axe masters are feared all over the battlefield. An anti-air tower is available to Orcs."
Hotkey=C
[A0DQ]
Name=Channel
Tip="Previous"
Ubertip="Previous page."
Hotkey=C
[A0DR]
Name=Channel
Tip="|cffFFD900Random Race|r"
Ubertip="You will get a random race."
Hotkey=C
[A0DS]
Name="Elemental Building"
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A0DM]
Name=Channel
Tip="|cffFFD900Choose Mech Coalition|r"
Ubertip="Clever goblin tinkers and machinists have gathered to assemble their own army! Their units have a lot of splash damage and high armor but lack ranged and magic attacks. They have a good tower with a bouncing attack."
Hotkey=C
[A0DN]
Name=Channel
Tip="|cffFFD900Choose Nature Force|r"
Ubertip="The forces of Nature itself are called to fight for their masters. Specializing in strong units and healing buffs, the Nature Race is slightly more expensive but better than the average race."
Hotkey=C
[A0E0]
Name=Channel
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A0DO]
Name=Channel
Tip="|cffFFD900Choose Elementals' Universe|r"
Ubertip="Elementals is the eldest race and exists since universe  appeared. Its units are pretty strong but also expensive. Elementals use control of elements for destroying its enemies and reinforcement magic for allies. This is the unique race that can decrease build time for its units. Has defense tower."
Hotkey=C
[A0DP]
Name=Channel
Tip="Next"
Ubertip="Next page."
Hotkey=C
[A0C5]
Name="SpcTakeDmg"
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A0C4]
Name=Channel
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A0BG]
Name="Pathing"
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A07E]
Name=Channel
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A07G]
Name="Summoned Unit"
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A07F]
Name=Channel
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A07J]
Name=Channel
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A07K]
Name=Channel
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A07H]
Name="LegendaryUnit"
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A07I]
Name=Channel
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A07N]
Name=Channel
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A07O]
Name=Channel
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A07M]
Name="Spcl AI"
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A08H]
Name="f_Hiden"
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A08X]
Name=Channel
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A08Y]
Name="Scroll of Stone Trigger"
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A09G]
Name="Gold Coin Trigger"
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A09H]
Name="DoubleSpawn"
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A444]
Name="TypeTwin"
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A02E]
Name="IsNeutral"
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A03U]
Name=Channel
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A03Q]
Name="SpcAtk"
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A0EZ]
Name=Channel
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A0F4]
Name=Channel
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A0F5]
Name="IsElem"
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A0GL]
Name="Desert race"
Tip="|cffFFD900Choose Desert Plains|r"
Ubertip="The Dwellers of Desert use mighty creatures and misterious rituals to confuse and overcome its enemies."
Hotkey=C
[A0GS]
Name="f_revived"
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[A0IC]
Name="Skip groupping"
Tip=|cffffcc00C|rhannel
Ubertip="Channels powerful warding magic."
Hotkey=C
[B022]
Bufftip="Parasite"
Buffubertip="This unit has been inflicted with a parasite; it will take damage over time, and if it dies while still afflicted, a minion will spawn from its corpse."
[B01T]
Bufftip=Targeted
Buffubertip=This unit is targeted by a spell
[B00H]
Bufftip=Double Damage
Buffubertip=This unit is dealing freaking wicked damage.
[B00I]
Bufftip=Quad Damage
Buffubertip=WTF?!

[A555]
Art=ReplaceableTextures\CommandButtons\BTNParasiteOn.blp
Buttonpos=,2
Hotkey=R
MissileHoming=1
Missilespeed=1900
Name=Parasite(leg troll)
Order=parasite
Orderoff=parasiteoff
Orderon=parasiteon
Tip=Pa|cffffcc00r|rasite
Ubertip="Afflicts a target enemy unit with a deadly parasite that deals 5 damage per second for 30 seconds. If the afflicted unit dies while under the effect of Parasite, a minor minion will spawn from its corpse."
Unart=ReplaceableTextures\CommandButtons\BTNParasiteOff.blp
UnButtonpos=,2
Unhotkey=R
Untip=|cffc3dbffRight-click to activate auto-casting.|r
Unubertip=|cffc3dbffRight-click to deactivate auto-casting.|r

[A556]
Art=ReplaceableTextures\CommandButtons\BTNEnsnare.blp
Buttonpos=,2
Hotkey=E
Missileart=Abilities\Spells\Orc\Ensnare\EnsnareMissile.mdl
MissileHoming=1
Missilespeed=1200
Name=Ensnare (Leg Troll)
Order=ensnare
Tip=|cffffcc00E|rnsnare
Ubertip=Causes a target enemy unit to be bound to the ground so that it cannot move for 12 seconds. Air units that are ensnared can be attacked as though they were land units.

[A666]
Art=ReplaceableTextures\PassiveButtons\PASBTNCleavingAttack.blp
Buttonpos=1,2
Name=Cleaving Attack Twins
SpecialArt=Abilities\Spells\Other\Cleave\CleaveDamageTarget.mdl
Specialattach=chest
Tip=Cleaving Attack
Ubertip=The creature strikes with such force that 25% of their damage strikes through to enemies near the primary attacked unit.

[A777]
Art=ReplaceableTextures\CommandButtons\BTNAvatar.blp
Buttonpos=,2
Name=Twin: Smile and Cry
Tip=Decay
Ubertip=Deals 10 spell damage per second to all non mechanical enemy units.

[A888]
Art=ReplaceableTextures\CommandButtons\BTNParasiteOn.blp
Buttonpos=,2
Hotkey=R
MissileHoming=1
Missilespeed=1900
Name=Parasite(twins)
Order=parasite
Orderoff=parasiteoff
Orderon=parasiteon
Tip=Pa|cffffcc00r|rasite
Ubertip="Afflicts a target enemy unit with a deadly parasite that deals 5 damage per second for 30 seconds. If the afflicted unit dies while under the effect of Parasite, a minor minion will spawn from its corpse."
Unart=ReplaceableTextures\CommandButtons\BTNParasiteOff.blp
UnButtonpos=,2
Unhotkey=R
Untip=|cffc3dbffRight-click to activate auto-casting.|r
Unubertip=|cffc3dbffRight-click to deactivate auto-casting.|r

[A999]
Art=ReplaceableTextures\PassiveButtons\PASBTNCriticalStrike.blp
Buttonpos=2,2
Name=Critical Strike(twins)
Tip=Critical Strike
Ubertip=Gives a 20% chance to do 2 times normal damage on an attack.

[AA11]
Art=ReplaceableTextures\PassiveButtons\PASBTNBash.blp
Buttonpos=2,2
Name=Bash
Order=bash
Tip=Bash
Ubertip=Gives a 15% chance that an attack will do 25 bonus damage and stun an opponent for 2 seconds.

[A889]
Animnames=spell,slam
Art=ReplaceableTextures\CommandButtons\BTNThunderclap.blp
Buttonpos=1,2
CasterArt=Abilities\Spells\Human\Thunderclap\ThunderClapCaster.mdl
Casterattach=origin
EffectArt=Abilities\Spells\Human\Thunderclap\ThunderClapCaster.mdl
Hotkey=C
Name=Twins:Thunder Clap
Order=thunderclap
ResearchArt=ReplaceableTextures\CommandButtons\BTNThunderclap.blp
Researchbuttonpos=1
Researchhotkey=C
Researchtip=Learn Thunder |cffffcc00C|rlap - [|cffffcc00Level %d|r]
Researchubertip="Slams the ground, dealing damage to and slowing the movement speed and attack rate of nearby enemy land units. |n|n|cffffcc00Level 1|r - 60 damage, 50% move, 50% attack. |n|cffffcc00Level 2|r - 100 damage, 50% move, 50% attack. |n|cffffcc00Level 3|r - 140 damage, 50% move, 50% attack."
TargetArt=Objects\Spawnmodels\Naga\NagaDeath\NagaDeath.mdl,Abilities\Spells\Human\Thunderclap\ThunderClapCaster.mdl
Tip=Thunder |cffffcc00C|rlap - [|cffffcc00Level 1|r]
Ubertip="Slams the ground, dealing 60 damage to nearby enemy land units and slowing their movement by 50% and attack rate by 50%."

[A6A6]
Art=ReplaceableTextures\CommandButtons\BTNParasiteOn.blp
Buttonpos=0,2
Hotkey=R
MissileHoming=1
Name=Parasite (fs)
Order=parasite
Orderoff=parasiteoff
Orderon=parasiteon
Tip=Pa|cffffcc00r|rasite (ot)
Ubertip="Afflicts a target enemy unit with a deadly parasite that deals 5 damage per second for 30 seconds. If the afflicted unit dies while under the effect of Parasite, a minor minion will spawn from its corpse."
Unart=ReplaceableTextures\CommandButtons\BTNParasiteOff.blp
UnButtonpos=0,2
Unhotkey=R
Untip=|cffc3dbffRight-click to activate auto-casting.|r
Unubertip=|cffc3dbffRight-click to deactivate auto-casting.|r

[A6F6]
Art=ReplaceableTextures\PassiveButtons\PASBTNScatterRockets.blp
Buttonpos=0,2
Missileart=Abilities\Weapons\WyvernSpear\WyvernSpearMissile.mdl
MissileHoming=1
Missilespeed=1000
Name=Barrage(Far Seer)
Tip=Barrage
Ubertip="Fires powerful rockets at nearby enemy air units, dealing 13 - 14 damage per hit."

[A6A7]
Art=ReplaceableTextures\CommandButtons\BTNSlowOn.blp
Buttonpos=0,2
CasterArt=Abilities\Spells\Human\Slow\SlowCaster.mdl
Hotkey=W
Name=Slow(dummy)
Order=slow
Orderoff=slowoff
Orderon=slowon
Tip=Slo|cffffcc00w|r
Ubertip=Slows a target enemy unit's attack rate by 25% and movement speed by 60%. |nLasts 60 seconds.
Unart=ReplaceableTextures\CommandButtons\BTNSlowOff.blp
UnButtonpos=0,2
Unhotkey=W
Untip=|cffc3dbffRight-click to activate auto-casting.|r
Unubertip=|cffc3dbffRight-click to deactivate auto-casting.|r

[A1BG]
Art=ReplaceableTextures\CommandButtons\BTNDalaranReject.blp
Buttonpos=0,2
Hotkey=R
MissileHoming=1
Missilespeed=1900
Name=Chaos Parasites
Order=parasite
Orderoff=parasiteoff
Orderon=parasiteon
Tip=Chaos Parasites
Ubertip="Afflicts a random organic enemy with parasites, causing it to attack 10% slower and take 7.5 spell damage per second for up to 25 seconds. If the unit dies while afflicted, two Chaos Spawns will burst out of its body to fight nearby enemies, up to a minute|n|n|cffFFFF80Cooldown:|cffFFFF00 20 |rseconds"
Unart=ReplaceableTextures\CommandButtons\BTNDalaranReject.blp
UnButtonpos=0,2
Unhotkey=R
Untip=|cffc3dbffRight-click to activate auto-casting.|r
Unubertip=|cffc3dbffRight-click to deactivate auto-casting.|r

[A1BF]
Art=ReplaceableTextures\CommandButtons\BTNRunedBracers.blp
Name=Chaos Spawn

[A1FL]
Art=ReplaceableTextures\PassiveButtons\PASBTNScatterRockets.blp
Buttonpos=0,2
Missileart=Abilities\Weapons\TuskarSpear\TuskarSpear.mdl
MissileHoming=1
Missilespeed=1100
Name=Barrage Igloo
Tip=Barrage
Ubertip="Fires powerful rockets at nearby enemy air units, dealing 13 - 14 damage per hit."

[A1FM]
Art=ReplaceableTextures\CommandButtons\BTNOrbOfLightning.blp
Missileart=Abilities\Weapons\TuskarSpear\TuskarSpear.mdl
Name=Igloo frost chance

[A1FY]
Animnames=spell,channel
Art=ReplaceableTextures\CommandButtons\BTNKiljaedin.blp
Buttonpos=1,1
Casterattach=origin
Hotkey=C
Name=Channel
Order=charm
Targetattach=origin
Tip=|cffFFD900Choose Ultimate Builder|r
Ubertip=Ultimate builder is the most unpredictable and powerful builder across all universe.It randomly attaches structures from other known races.

[A1CV]
Animnames=spell,channel
Art=ReplaceableTextures\CommandButtons\BTNDeathPact.blp
Buttonpos=2
CasterArt=Abilities\Spells\Undead\DeathPact\DeathPactTarget.mdl
Casterattach=origin
EffectArt=Abilities\Spells\Undead\DeathPact\DeathPactTarget.mdl
Hotkey=C
Name=Channel ( Assasin Target )
Order=roar
TargetArt=Abilities\Spells\Undead\DeathPact\DeathPactTarget.mdl
Targetattach=origin
Tip=|cffffcc00C|rhannel
Ubertip=Channels powerful warding magic.

[A1FV]
Art=ReplaceableTextures\PassiveButtons\PASBTNDemolish.blp
Buttonpos=1,1
Name=Demolisher
Order=demolish
Tip=Demolish
Ubertip=Causes attacks to do 2 times normal damage to buildings.

[A108]
Art=ReplaceableTextures\CommandButtons\BTNChestOfGold.blp
Buttonpos=0,2
CasterArt=Abilities\Spells\Other\Transmute\PileofGold.mdl
Casterattach=origin
Order=sellgold
Tip=Sell
Ubertip="|cffffd694Vietnamese|r: |cffff9999Khong hoan` lai. vang` va` go~ dau, chi? de? xoa' nha` thoi.|r|n|cffffd694English|r: |cffff9999Gold and wood are not refundable, only for remove house.|r"

[A208]
Art=ReplaceableTextures\CommandButtons\BTNSpellBookBLS.blp
Buttonpos=3,1
Casterattach=origin
Name=Change Color
Order=chcolor
Tip=Change Color
Ubertip="|cffffa3fcChange Builder Color"
Hotkey=F

[A308]
Art=ReplaceableTextures\CommandButtons\BTNSpellBookBLS.blp
Buttonpos=2,1
Casterattach=origin
Name=Change Wing
Order=chwing
Tip=Change Wing
Ubertip="|cffffa3fcChange Builder Wing|n|r|cffffa3fcCooldown|r: 7s"
Hotkey=D

[A902]
Art=ReplaceableTextures\CommandButtons\BTNIceCrownObelisk.blp
Buttonpos=1,2
Casterattach=origin
Name=Move Tower
Order=evileye
Tip=Movement Obelisk - [|cffFFcc00V|r]
Ubertip="|cffffd694Vietnamese|r: |cffff9999Dung` de? dich. chuyen? nha` ra cho~ khac'.|r|n|cffffd694English|r: |cffff9999Used to move house to another place.|r|n|cffffd694Lost Cost|r: Default |cfffffa9950 gold|r, Use the command |cffff8cfd-cmtg|r to see if the host has set a different value"
Hotkey=V

[A127]
Art=,
EditorSuffix=1 Star
Name=Sphere
TargetArt=war3mapImported\Star_Overhead1.MDX
Targetattach=overhead
Targetattachcount=1

[A227]
Art=,
EditorSuffix=2 Star
Name=Sphere
TargetArt=war3mapImported\Star_Overhead2.MDX
Targetattach=overhead
Targetattachcount=1

[A327]
Art=,
EditorSuffix=3 Star
Name=Sphere
TargetArt=war3mapImported\Star_Overhead3.MDX
Targetattach=overhead
Targetattachcount=1

[A427]
Art=,
EditorSuffix=4 Star
Name=Sphere
TargetArt=war3mapImported\Star_Overhead4.MDX
Targetattach=overhead
Targetattachcount=1

[A527]
Art=,
EditorSuffix=5 Star
Name=Sphere
TargetArt=war3mapImported\Star_Overhead5.MDX
Targetattach=overhead
Targetattachcount=1

[A627]
Art=,
EditorSuffix=6 Star
Name=Sphere
TargetArt=war3mapImported\Star_Overhead6.MDX
Targetattach=overhead
Targetattachcount=1
