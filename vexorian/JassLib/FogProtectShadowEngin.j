library FogProtectShadowEngine needs AMHSCache
//*************************************************************************
//*                                                                       *                                          *
//*                      FOG PROTECT & SHADOW ENGINE v1.20                *
//*                      CONFIGURATION SETTINGS START HERE                *                     *
//*                                                                       *                                          *
//*************************************************************************
globals

// This is the default alpha value used for shadows. From visual experimentation the alpha
// value of Blizzards shadows appear to be 180. If desire you can change the value
// NOTE: If you wish you can change the alpha value of a shadow using the SetShadowAlpha
// method at any time, this is just the default value when the shadow is created
private constant integer shadowalpha_default = 180

// This is the interval period used by the system. 0.025 should be the LOWEST value, anything
// lower is pointless since 0.025 matches Wc3's maximum FPS (60 fp/s). If the system is
// causing lag then you should increase it to a value between 0.025 and 0.05. Anything higher
// then 0.05 is seriously not recommended since it will look visually choppy.
// NOTE: The system is INCREDIBLY efficient and it uses the struct-loop system (no H2I/attach
// methods are used for the timer interval) so in 99% of cases you should never have to increase
// the period
private constant real period = 0.025

// By default Blizzard shadows are disabled when a unit dies (note when a unit dies, this doesn't
// necessarily mean when its removed from the game. If a unit is removed from the by killunit
// or removeunit the shadow is destroyed, however should a hero die then the shadow will not
// be removed, instead it will be disabled. A units death is evaluated by the 
// GetUnitState(u,UNIT_STATE_MAX_LIFE) <= 0.405. Should the unit be revived by a spell such
// as reincarnation then its shadow will return back to what it was before it died

// Setting this to false will mean the shadow will still remain even if the unit dies. NOTE: If
// hideshadowondeath is enabled it can be detect with the GetShadowVisibility method, so this
// means you would need to take care of when a unit dies when dealing with showing/hiding shadows
 
private constant boolean hideshadowondeath = true

endglobals
//*************************************************************************
//*                                                                       *                                          *
//*                            FOG PROTECT & SHADOW ENGINE v1.20          *                       *
//*                            CONFIGURATION SETTINGS END HERE            *                        *
//*                                                                       *                                          *
//*************************************************************************

// Create has used to store buffs

globals
    private gamecache fp_datacache
    private boolean cachefirst = true
    private trigger RegisterUnit = CreateTrigger()
    private group g = CreateGroup()
    
    //Stack for shadow struct
    private integer array s_StructStack
    private integer array s_StructFreeStack
    private integer s_StructNumber = 0
    private integer s_StructFreeNumber = -1
    //Stack for  unit/graphic struct
    private integer array gp_StructStack
    private integer array gp_StructFreeStack
    private integer gp_StructNumber = 0
    private integer gp_StructFreeNumber = -1    
endglobals

private function H2I takes handle h returns integer
return GetHandleId(h)
endfunction

private function InitializeDataCache takes nothing returns nothing
call FlushGameCache(InitGameCache("fp_datacache"))
set fp_datacache=InitGameCache("fp_datacache")
endfunction

//! runtextmacro AMHS_HandleInit()

//Shadow Struct -> Public Use
struct shadow
    readonly real x
    readonly real y
    readonly real centrex
    readonly real centrey
    readonly real width
    readonly real height
    readonly integer r
    readonly integer g
    readonly integer b
    readonly integer a
    readonly unit u
    readonly string path
    readonly image i
    readonly boolean enabled = true
    readonly boolean deadunit = false
    readonly integer position //position in relation to struct stack

    
    static method Create takes unit u, real centrex, real centrey, real width, real height, integer alpha, string path returns shadow
    local shadow s = shadow.create()
    local real x = GetUnitX(u)
    local real y = GetUnitY(u)
    set s.x = x
    set s.y = y
    set s.width = width
    set s.height = height
    set s.a = alpha
    set s.u = u
    set s.centrex = centrex
    set s.centrey = centrey
    set s.path = path
    
    if path == "normal" or path == "Normal" or path == "NORMAL" or path == "Shadow" or path == "shadow" or path == "SHADOW" then
        set s.path = "ReplaceableTextures\\Shadows\\Shadow.blp"
    elseif path == "flyer" or path == "Flyer" or path == "FLYER" or path == "ShadowFlyer" or path == "shadowflyer" or path == "SHADOWFLYER" then
        set s.path = "Textures\\Shadow.blp"
    elseif path == "none" or path == "None" or path == "NONE" then
        set s.path = ""
    endif
    set s.i = CreateImage(s.path, width, height, 0, x - (width / 2), y - (height / 2), 0, centrex, centrey, 0, 2) // image type indicator
    call SetImageRenderAlways(s.i, true)
    call ShowImage(s.i, true)
    call SetImageColor(s.i,255,255,255,alpha)
    set s.r = 255
    set s.g = 255
    set s.b = 255
    set s.a = 180
    //Update Struct Stack
    if s_StructFreeNumber<0 then
        set s.position = s_StructNumber
        set s_StructStack[s_StructNumber] = s
        set s_StructNumber = s_StructNumber+1
    else
        set s.position = s_StructFreeStack[s_StructFreeNumber]
        set s_StructStack[s.position] = s
        set s_StructFreeStack[s_StructFreeNumber] = -1
        set s_StructFreeNumber = s_StructFreeNumber-1
    endif
    //! runtextmacro AMHS_StoreStructShadow("s.u","s")
    return s
    endmethod
    
    public method ChangeDeadUnit takes boolean flag returns nothing
        set this.deadunit = flag
    endmethod
    
    method GetShadowPath takes nothing returns string
        return this.path
    endmethod
    
    method GetShadowAlpha takes nothing returns integer
        return this.a
    endmethod
    
    method GetShadowRed takes nothing returns integer
        return this.r
    endmethod
    
    method GetShadowGreen takes nothing returns integer
        return this.g
    endmethod
    
    method GetShadowBlue takes nothing returns integer
        return this.b
    endmethod
    
    method GetShadowCentreX takes nothing returns real
        return this.centrex
    endmethod
    
    method GetShadowCentreY takes nothing returns real
        return this.centrey
    endmethod
    
    method GetShadowHeight takes nothing returns real
        return this.height
    endmethod
    
    method GetShadowWidth takes nothing returns real
        return this.width
    endmethod
    
    method GetShadowUnit takes nothing returns unit
        return this.u
    endmethod
    
    method GetDefaultShadowCentreX takes nothing returns real
        local string s = UnitId2String(GetUnitTypeId(this.u))
        if GetStoredBoolean(fp_datacache,s,"exist") == true then
            return GetStoredReal(fp_datacache,s,"centrex")
        else
            return I2R(0)
        endif
    endmethod
    
    method GetDefaultShadowCentreY takes nothing returns real
        local string s = UnitId2String(GetUnitTypeId(this.u))
        if GetStoredBoolean(fp_datacache,s,"exist") == true then
            return GetStoredReal(fp_datacache,s,"centrey")
        else
            return I2R(0)
        endif
    endmethod
    
    method GetDefaultShadowHeight takes nothing returns real
        local string s = UnitId2String(GetUnitTypeId(this.u))
        if GetStoredBoolean(fp_datacache,s,"exist") == true then
            return GetStoredReal(fp_datacache,s,"height")
        else
            return I2R(0)
        endif
    endmethod
    
    method GetDefaultShadowWidth takes nothing returns real
        local string s = UnitId2String(GetUnitTypeId(this.u))
        if GetStoredBoolean(fp_datacache,s,"exist") == true then
            return GetStoredReal(fp_datacache,s,"width")
        else
            return I2R(0)
        endif
    endmethod
    
    method GetDefaultShadowPath takes nothing returns string
        local string s = UnitId2String(GetUnitTypeId(this.u))
        if GetStoredBoolean(fp_datacache,s,"exist") == true then
            return GetStoredString(fp_datacache,s,"path")
        else
            return ""
        endif
    endmethod
    
    method SetShadowColor takes integer red, integer green, integer blue, integer alpha returns nothing
        call SetImageColor(this.i,red,green,blue,alpha)
        set this.r = red
        set this.g = green
        set this.b = blue
        set this.a = alpha
    endmethod
    
    method SetShadowAlpha takes integer alpha returns nothing
        call SetImageColor(this.i,this.r,this.g,this.b,alpha)
        set this.a = alpha
    endmethod
    
    method SetShadowVisibility takes boolean visibility returns nothing
        call ShowImage(this.i,visibility)
        set this.enabled = visibility
    endmethod
    
    method SetShadowImage takes string path returns nothing
        local string truepath = path
        call DestroyImage(this.i)
        set this.i = null
        if path == "normal" or path == "Normal" or path == "NORMAL" then
            set truepath = "ReplaceableTextures\\Shadows\\Shadow.blp"
        elseif path == "flyer" or path == "Flyer" or path == "FLYER" then
            set truepath = "Textures\\Shadow.blp"
        elseif path == "none" or path == "None" or path == "NONE" then
            set truepath = ""
        endif
        set this.i = CreateImage(truepath, this.width, this.height, 0, this.x - (this.width / 2), this.y - (this.height / 2), 0, this.centrex, this.centrey, 0, 2) // image type indicator
        set this.path = truepath
        call SetImageRenderAlways(this.i, true)
        call ShowImage(this.i, true)
        call SetImageColor(this.i,this.r,this.g,this.b,this.a)
     endmethod
     
    method SetShadowImageEx takes string path, real centrex, real centrey, real width, real height returns nothing
        local string truepath = path
        call DestroyImage(this.i)
        set this.i = null
        if path == "normal" or path == "Normal" or path == "NORMAL" then
            set truepath = "ReplaceableTextures\\Shadows\\Shadow.blp"
        elseif path == "flyer" or path == "Flyer" or path == "FLYER" then
            set truepath = "Textures\\Shadow.blp"
        elseif path == "none" or path == "None" or path == "NONE" then
            set truepath = ""
        endif
        set this.i = CreateImage(truepath, width, height, 0, this.x - (width / 2), this.y - (height / 2), 0, centrex, centrey, 0, 2) // image type indicator
        set this.path = truepath
        set this.centrex = centrex
        set this.centrey = centrey
        set this.width = width
        set this.height = height
        call SetImageRenderAlways(this.i, true)
        call ShowImage(this.i, true)
        call SetImageColor(this.i,this.r,this.g,this.b,this.a)    
     endmethod
     
    method SetShadowDimeansions  takes real centrex, real centrey, real width, real height returns nothing
        call DestroyImage(this.i)
        set this.i = null
        set this.i = CreateImage(this.path, width, height, 0, this.x - (width / 2), this.y - (height / 2), 0, centrex, centrey, 0, 2) // image type indicator
        set this.centrex = centrex
        set this.centrey = centrey
        set this.width = width
        set this.height = height
        call SetImageRenderAlways(this.i, true)
        call ShowImage(this.i, true)
        call SetImageColor(this.i,this.r,this.g,this.b,this.a)
    endmethod
    
    method onDestroy takes nothing returns nothing
    call DestroyImage(this.i)
    set this.i = null
    set s_StructStack[this.position] = -1
    if (this.position>=(s_StructNumber-1)) then
        set s_StructNumber = s_StructNumber-1
    else
        set s_StructFreeNumber = s_StructFreeNumber+1
        set s_StructFreeStack[s_StructFreeNumber] = this.position
    endif
    //! runtextmacro AMHS_DestroyStructShadow("this.u")
    set this.u = null
    set this.position = -1
    endmethod
     
endstruct

//Struct for managing SetUnitVertexColor/SetUnitScale
struct unitgraphic
    readonly unit u
    readonly integer r
    readonly integer g
    readonly integer b
    readonly integer a
    readonly real x
    readonly real y
    readonly real z
    readonly boolean enabled
    readonly integer position //position in relation to struct stack
    
    static method Create takes unit u, integer red, integer green, integer blue, integer alpha, real x, real y, real z returns unitgraphic
    local unitgraphic gp = unitgraphic.create()
    set gp.r = red
    set gp.g = green
    set gp.b = blue
    set gp.a = alpha
    set gp.x = x
    set gp.y = y
    set gp.z = z
    set gp.u = u
    set gp.enabled = true
    //Update Struct Stack
    if gp_StructFreeNumber<0 then
        set gp.position = gp_StructNumber
        set gp_StructStack[gp_StructNumber] = gp
        set gp_StructNumber = gp_StructNumber+1
    else
        set gp.position = gp_StructFreeStack[gp_StructFreeNumber]
        set gp_StructStack[gp.position] = gp
        set gp_StructFreeStack[gp_StructFreeNumber] = -1
        set gp_StructFreeNumber = gp_StructFreeNumber-1
    endif
    //! runtextmacro AMHS_StoreStructUnitGraphic("gp.u","gp")
    set u = null
    return gp
    endmethod
    
    method SetVertexColor takes integer red, integer green, integer blue, integer alpha returns nothing
    set this.r = red
    set this.g = green
    set this.b = blue
    set this.a = alpha
    endmethod
    
    method SetVertexColorBJ takes real red, real green, real blue, real transparency returns nothing
    call this.SetVertexColor(PercentTo255(red), PercentTo255(green), PercentTo255(blue), PercentTo255(100.0-transparency))
    endmethod
    
    method SetScale takes real scaleX, real scaleY, real scaleZ returns nothing
    set this.x = scaleX
    set this.y = scaleY
    set this.z = scaleZ
    endmethod
    
    method SetScalePercent takes real percentScaleX, real percentScaleY, real percentScaleZ returns nothing
    call this.SetScale(percentScaleX * 0.01, percentScaleY * 0.01, percentScaleZ * 0.01)
    endmethod

    method SetGraphicVisibility takes boolean visibility returns nothing
        set this.enabled = visibility
    endmethod
    
    method GetScaleX takes nothing returns real
        return this.x
    endmethod
    
    method GetScaleY takes nothing returns real
        return this.y
    endmethod
    
    method GetScaleZ takes nothing returns real
        return this.z
    endmethod
    
    method GetVertexColorRed takes nothing returns integer
        return this.r
    endmethod
    
    method GetVertexColorGreen takes nothing returns integer
        return this.g
    endmethod
    
    method GetVertexColorBlue takes nothing returns integer
        return this.b
    endmethod
    
    method GetVertexColorAlpha takes nothing returns integer
        return this.a
    endmethod
    
    method GetGraphicVisibility takes nothing returns boolean
        return this.enabled
    endmethod
    
    method onDestroy takes nothing returns nothing
    set gp_StructStack[this.position] = -1
    if (this.position>=(gp_StructNumber-1)) then
        set gp_StructNumber = gp_StructNumber-1
    else
        set gp_StructFreeNumber = gp_StructFreeNumber+1
        set gp_StructFreeStack[gp_StructFreeNumber] = this.position
    endif   
    set this.position = -1
    //! runtextmacro AMHS_DestroyStructUnitGraphic("this.u")
    set this.u = null
    endmethod
   
endstruct

//Public Functions

function RegisterUnitShadow takes integer UnitID, real centrex, real centrey, real height, real width, string ShadowType returns nothing
local string s
if cachefirst then
    call InitializeDataCache()
    set cachefirst = false
endif
set s = UnitId2String(UnitID)
call StoreBoolean(fp_datacache,s,"exist",true)
call StoreReal(fp_datacache,s,"height",height)
call StoreReal(fp_datacache,s,"width",width)
call StoreReal(fp_datacache,s,"centrex",centrex)
call StoreReal(fp_datacache,s,"centrey",centrey)
call StoreString(fp_datacache,s,"path",ShadowType)
endfunction

function RegisterUnitTintEx takes integer UnitID, integer red, integer green, integer blue, integer alpha returns nothing
local string s
if cachefirst then
    call InitializeDataCache()
    set cachefirst = false
endif
set s = UnitId2String(UnitID)
call StoreBoolean(fp_datacache,s,"customtint",true)
call StoreInteger(fp_datacache,s,"tintred",red)
call StoreInteger(fp_datacache,s,"tintgreen",green)
call StoreInteger(fp_datacache,s,"tintblue",blue)
call StoreInteger(fp_datacache,s,"tintalpha",alpha)
endfunction

function RegisterUnitTint takes integer UnitID, integer red, integer green, integer blue returns nothing
call RegisterUnitTintEx(UnitID,red,green,blue,255)
endfunction

function RegisterUnitScaleEx takes integer UnitID, real scaleX, real scaleY, real scaleZ returns nothing
local string s
if cachefirst then
    call InitializeDataCache()
    set cachefirst = false
endif
set s = UnitId2String(UnitID)
call StoreBoolean(fp_datacache,s,"customscale",true)
call StoreReal(fp_datacache,s,"scalex",scaleX)
call StoreReal(fp_datacache,s,"scaley",scaleY)
call StoreReal(fp_datacache,s,"scalez",scaleZ)
endfunction

function RegisterUnitScale takes integer UnitID, real scale returns nothing
call RegisterUnitScaleEx(UnitID,scale,scale,scale)
endfunction

globals
    private integer array bs_StructStack
    private integer array bs_StructFreeStack
    private integer bs_StructNumber = 0
    private integer bs_StructFreeNumber = -1  
endglobals

private struct buffstruct
    integer BuffID
    real x = 0
    real y = 0 
    real z = 0
    real increasex
    real increasey
    real increasez
    integer increasecount
    real SizeDurationIncrease = 0
    integer position
    real Duration
    integer Level = 0
    
    static method Create takes integer BuffID, real x, real y, real z, real SizeDurationIncrease, real Duration, integer Level returns buffstruct
    local buffstruct bs = buffstruct.create()
    set bs.x = x
    set bs.y = y
    set bs.z = z
    set bs.SizeDurationIncrease = SizeDurationIncrease
    set bs.BuffID = BuffID
    set bs.Duration = Duration
    set bs.Level = Level
    if SizeDurationIncrease > 0 then
        set bs.increasecount = R2I(SizeDurationIncrease/period)
        set bs.increasex = bs.x/bs.increasecount
        set bs.increasey = bs.y/bs.increasecount
        set bs.increasez = bs.z/bs.increasecount
    endif
    //Update Struct Stack
    if bs_StructFreeNumber<0 then
        set bs.position = bs_StructNumber
        set bs_StructStack[bs_StructNumber] = bs
        set bs_StructNumber = bs_StructNumber+1
    else
        set bs.position = bs_StructFreeStack[bs_StructFreeNumber]
        set bs_StructStack[bs.position] = bs
        set bs_StructFreeStack[bs_StructFreeNumber] = -1
        set bs_StructFreeNumber = bs_StructFreeNumber-1
    endif
    return bs
    endmethod
    
endstruct

    
function RegisterBuffScaleEx takes integer BuffID, real scaleX, real scaleY, real scaleZ, real SizeDurationIncrease, real Duration, integer Level returns nothing
local buffstruct bs
set bs = buffstruct.Create(BuffID,scaleX,scaleY,scaleZ,SizeDurationIncrease,Duration,Level)
endfunction

function RegisterBuffScale takes integer BuffID, real scale,real SizeDurationIncrease, real Duration, integer Level returns nothing
call RegisterBuffScaleEx(BuffID,scale,scale,scale,SizeDurationIncrease,Duration,Level)
endfunction

function SetUnitVertexColorAMHS takes unit whichUnit, integer red, integer green, integer blue, integer alpha returns nothing
local unitgraphic gp
if whichUnit != null then
    //! runtextmacro AMHS_GetStructUnitGraphic("whichUnit","gp")
    call gp.SetVertexColor(red,green,blue,alpha)
endif
endfunction

function SetUnitVertexColorBJAMHS takes unit whichUnit, real red, real green, real blue, real transparency returns nothing
    call SetUnitVertexColorAMHS(whichUnit, PercentTo255(red), PercentTo255(green), PercentTo255(blue), PercentTo255(100.0-transparency))
endfunction

function SetUnitScaleAMHS takes unit whichUnit, real scaleX, real scaleY, real scaleZ returns nothing
local unitgraphic gp
if whichUnit != null then
    //! runtextmacro AMHS_GetStructUnitGraphic("whichUnit","gp")
    call gp.SetScale(scaleX,scaleY,scaleZ)
endif
endfunction

function SetUnitScalePercentAMHS takes unit whichUnit, real percentScaleX, real percentScaleY, real percentScaleZ returns nothing
    call SetUnitScaleAMHS(whichUnit, percentScaleX * 0.01, percentScaleY * 0.01, percentScaleZ * 0.01)
endfunction

function GetUnitShadow takes unit u returns integer
    local shadow sh
	if u == null then
        return 0
    endif
    //! runtextmacro AMHS_GetStructShadow("u","sh")
	return sh
endfunction

function GetUnitGraphic takes unit u returns integer
	local unitgraphic gp
    if u == null then
        return 0
    endif
	//! runtextmacro AMHS_GetStructUnitGraphic("u","gp")
	return gp
endfunction

//End Public Functions

private function AddUnits takes unit u returns nothing
    local string s
    local string path
    local real centrex
    local real centrey
    local real height
    local real width
    local integer red = 255
    local integer green = 255
    local integer blue = 255
    local integer alpha = 255
    local real x = 1
    local real y = 1
    local real z = 1
    local shadow sh
    local unitgraphic gp
    set s = UnitId2String(GetUnitTypeId(u))
    if GetStoredBoolean(fp_datacache,s,"exist") == true then
        set path = GetStoredString(fp_datacache,s,"path")
        set centrex = GetStoredReal(fp_datacache,s,"centrex")
        set centrey = GetStoredReal(fp_datacache,s,"centrey")
        set height = GetStoredReal(fp_datacache,s,"height")
        set width = GetStoredReal(fp_datacache,s,"width")
        set sh = shadow.Create(u,centrex,centrey,width,height,shadowalpha_default,path)
    else
        call BJDebugMsg("|cFFFF0000AMHS ERROR: Unable to create shadow for unit with UnitType ID " + s)
    endif

    if GetStoredBoolean(fp_datacache,s,"customtint") then
        set red = GetStoredInteger(fp_datacache,s,"tintred")
        set green = GetStoredInteger(fp_datacache,s,"tintgreen")
        set blue = GetStoredInteger(fp_datacache,s,"tintblue")
        set alpha = GetStoredInteger(fp_datacache,s,"tintalpha")
    endif
    if GetStoredBoolean(fp_datacache,s,"customscale") then
        set x = GetStoredReal(fp_datacache,s,"scalex")
        set y = GetStoredReal(fp_datacache,s,"scaley")
        set z = GetStoredReal(fp_datacache,s,"scalez")
    endif
    set gp = unitgraphic.Create(u,red,green,blue,alpha,x,y,z)
    set u = null
endfunction

private function ShadowGraphicManager takes nothing returns nothing
    local unitgraphic gp
    local shadow s
    local integer counter = 0
    local real l1 = GetRandomReal(1000,10000000000)
    local real l2 = GetRandomReal(1000,10000000000)
    local real l3 = GetRandomReal(1000,10000000000)
    local player p = GetLocalPlayer()
    loop
        set gp = gp_StructStack[counter]
        if integer(gp) > -1 then
            if GetUnitTypeId(gp.u) == 0 then
                call gp.destroy()
            else
                if IsUnitVisible(gp.u, p) then
                    if gp.enabled then
                        call SetUnitVertexColor(gp.u,gp.r,gp.g,gp.b,gp.a)
                        call SetUnitScale(gp.u,gp.x,gp.y,gp.z)
                    else
                        call SetUnitVertexColor(gp.u,0,0,0,0)
                        call SetUnitScale(gp.u,-l1,-l2,-l3)
                    endif
                elseif GetLocalPlayer() == p then
                    call SetUnitVertexColor(gp.u,0,0,0,0)
                    call SetUnitScale(gp.u,-l1,-l2,-l3)
                endif
            endif
        endif
        set counter = counter + 1
        exitwhen counter >= gp_StructNumber
    endloop
        
    set counter = 0
    loop
        set s = s_StructStack[counter]
        if integer(s) > -1 then
            if GetUnitTypeId(s.u) == 0 then
                call s.destroy()
            else
                if GetUnitState(s.u,UNIT_STATE_MAX_LIFE) > 0.405 and s.deadunit then
                    call s.ChangeDeadUnit(false)
                    call s.SetShadowVisibility(true)
                endif
                if IsUnitVisible(s.u, p) then
                    if s.enabled then
                        call ShowImage(s.i,true)
                    else
                        call ShowImage(s.i,false)
                    endif
                elseif GetLocalPlayer() == p then
                    call ShowImage(s.i,false)
                endif
                call SetImagePosition(s.i,GetUnitX(s.u),GetUnitY(s.u),0)
            endif
        endif
        set counter = counter + 1
        exitwhen counter >= s_StructNumber
    endloop
endfunction

private function GroupAdder takes nothing returns nothing
    call AddUnits(GetEnumUnit())
endfunction

private function TriggerAdder takes nothing returns boolean
    call AddUnits(GetTriggerUnit())
    return true
endfunction

private function AntiLeak takes nothing returns boolean
    return true
endfunction

private function FinalGroup takes nothing returns nothing
    call GroupAddUnit(g,GetEnumUnit())
endfunction

globals
    private integer array bc_StructStack
    private integer array bc_StructFreeStack
    private integer bc_StructNumber = 0
    private integer bc_StructFreeNumber = -1  
endglobals

private struct buffcheck
    unit u
    integer position
    integer buffstructid
    integer unitgraphicid
    integer counter
    boolean increasing
    boolean decreasing
    real timeelapsed

    static method Create takes unit u, integer buffstructid, integer unitgraphicid returns buffcheck
    local buffcheck bcs = buffcheck.create()
    local buffstruct bs = buffstructid
    set bcs.u = u
    set bcs.buffstructid = buffstructid
    set bcs.unitgraphicid = unitgraphicid
    set bcs.counter = 0
    set bcs.timeelapsed = 0
    if bs.SizeDurationIncrease > 0 then
        set bcs.increasing = true
    else
        set bcs.increasing = false
    endif
    //Update Struct Stack
    if bc_StructFreeNumber<0 then
        set bcs.position = bc_StructNumber
        set bc_StructStack[bc_StructNumber] = bcs
        set bc_StructNumber = bc_StructNumber+1
    else
        set bcs.position = bs_StructFreeStack[bc_StructFreeNumber]
        set bc_StructStack[bcs.position] = bcs
        set bc_StructFreeStack[bc_StructFreeNumber] = -1
        set bc_StructFreeNumber = bc_StructFreeNumber-1
    endif
    return bcs
    endmethod
    
    method onDestroy takes nothing returns nothing
    set bc_StructStack[this.position] = -1
    if (this.position>=(bc_StructNumber-1)) then
        set bc_StructNumber = bc_StructNumber-1
    else
        set bc_StructFreeNumber = bc_StructFreeNumber+1
        set bc_StructFreeStack[bc_StructFreeNumber] = this.position
    endif
    //! runtextmacro AMHS_DestroyStructShadow("this.u")
    set this.u = null
    set this.position = -1
    endmethod
endstruct

private function BuffManager takes nothing returns nothing
local unit u = GetEnumUnit()
local integer counter = 0
local integer lastbuffid = 0
local buffstruct bs
local buffcheck bcs
local unitgraphic gp
loop
    if (bs_StructStack[counter]) > -1 then
        set bs = bs_StructStack[counter]

        if GetUnitAbilityLevel(u, bs.BuffID) == bs.Level and bs.BuffID != lastbuffid then
            set lastbuffid = bs.BuffID
            //! runtextmacro AMHS_GetStructBuff("u", "bcs")
            if bcs <= 0 then
                //! runtextmacro AMHS_GetStructUnitGraphic("u","gp")
                if bs.SizeDurationIncrease == 0 then
                    call gp.SetScale(gp.x + bs.x,gp.y + bs.y,gp.z + bs.z)
                endif
                set bcs = buffcheck.Create(u,bs,gp)
                //! runtextmacro AMHS_StoreStructBuff("u", "bcs")
            endif
        endif
    endif
    set counter = counter + 1
    exitwhen counter >= bs_StructNumber
endloop
endfunction

private function BuffEnumerator takes nothing returns nothing
local group g = CreateGroup()
local integer counter = 0
local buffcheck bcs
local buffstruct bs
local unitgraphic gp
call GroupEnumUnitsInRect(g,bj_mapInitialPlayableArea,Condition(function AntiLeak))
call ForGroup(g,function BuffManager)
call DestroyGroup(g)
set g = null
loop
    if (bc_StructStack[counter]) > -1 then
        set bcs = bc_StructStack[counter]
        set bs = bcs.buffstructid
        set gp = bcs.unitgraphicid
        set bcs.timeelapsed = bcs.timeelapsed + period
        if GetUnitAbilityLevel(bcs.u, bs.BuffID) == bs.Level then
            if bs.Duration - bcs.timeelapsed <= bs.SizeDurationIncrease then
                set bcs.decreasing = true
            endif
            if bcs.increasing == true then
                if (bcs.counter != bs.increasecount) then
                    call gp.SetScale(gp.x + bs.increasex,gp.y + bs.increasey,gp.z + bs.increasez)
                    set bcs.counter = bcs.counter + 1
                else
                    set bcs.increasing = false
                    set bcs.counter = 0
                endif
            endif
            elseif (bcs.decreasing == true) then
                if (bcs.counter != bs.increasecount) then
                    call gp.SetScale(gp.x - bs.increasex,gp.y - bs.increasey,gp.z - bs.increasez)
                    set bcs.counter = bcs.counter + 1
                else
                    set bcs.decreasing = false
                endif
        else
            call bcs.destroy()
        endif
    endif
    set counter = counter + 1
    exitwhen counter >= bc_StructNumber
endloop    

endfunction

private function HideUnitOnDeath takes nothing returns nothing
    local shadow sh
    //! runtextmacro AMHS_GetStructShadow("GetTriggerUnit()","sh") 
    call sh.SetShadowVisibility(false)
    call sh.ChangeDeadUnit(true)
endfunction

function Trig_Shadow_Engine_Actions takes nothing returns nothing
    local timer t = CreateTimer()
    local timer t2 = CreateTimer()
    local integer i = 0
    local group tempgroup
    local trigger hideondeath
    //! runtextmacro AMHS_StructInitializeLine()
    if cachefirst then
        call InitializeDataCache()
        set cachefirst = false
    endif
    loop
        exitwhen i > 15
        set tempgroup = CreateGroup()
        call GroupEnumUnitsOfPlayer(tempgroup, Player(i), Filter( function AntiLeak))
        call ForGroup(tempgroup, function FinalGroup)
        call DestroyGroup(tempgroup)
        set tempgroup = null
        set i = i + 1
    endloop
    call ForGroup(g,function GroupAdder)
    call DestroyGroup(g)
    call TriggerRegisterEnterRectSimple(RegisterUnit, bj_mapInitialPlayableArea)
    call TriggerAddCondition(RegisterUnit, Condition (function TriggerAdder))
    call TimerStart(t,period,true,function ShadowGraphicManager)
    call TimerStart(t2,period,true, function BuffEnumerator)
    if hideshadowondeath then
        set hideondeath = CreateTrigger()
        call TriggerAddAction(hideondeath, function HideUnitOnDeath)
        call TriggerRegisterAnyUnitEventBJ(hideondeath, EVENT_PLAYER_UNIT_DEATH )
    endif
    set t = null
    set tempgroup = null
    set hideondeath = null
endfunction

//===========================================================================
//function InitTrig_Fog_Protect_and_Shadow_Engine takes nothing returns nothing
//    set gg_trg_Fog_Protect_and_Shadow_Engine = CreateTrigger(  )
//    call TriggerRegisterTimerEventSingle(gg_trg_Fog_Protect_and_Shadow_Engine, 0.00)
//    call TriggerAddAction( gg_trg_Fog_Protect_and_Shadow_Engine, function Trig_Shadow_Engine_Actions )
//endfunction
endlibrary