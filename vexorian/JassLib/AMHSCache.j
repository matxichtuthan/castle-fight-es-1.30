library AMHSCache

//! textmacro AMHS_HandleInit
globals
private gamecache fp_handlecache
endglobals

private function InitializeHandleCache takes nothing returns nothing
call FlushGameCache(InitGameCache("fp_handlecache"))
set fp_handlecache=InitGameCache("fp_handlecache")
endfunction
//! endtextmacro

//! textmacro AMHS_StoreStructShadow takes UNITID, STRUCTID
call StoreInteger(fp_handlecache,I2S(H2I($UNITID$)),"0",$STRUCTID$)
//! endtextmacro

//! textmacro AMHS_GetStructShadow takes UNITID, STRUCTID
set $STRUCTID$ = GetStoredInteger(fp_handlecache,I2S(H2I($UNITID$)),"0")
//! endtextmacro

//! textmacro AMHS_DestroyStructShadow takes UNITID
call StoreInteger(fp_handlecache,I2S(H2I($UNITID$)),"0",0)
//! endtextmacro 

//! textmacro AMHS_StoreStructUnitGraphic takes UNITID, STRUCTID
call StoreInteger(fp_handlecache,I2S(H2I($UNITID$)),"1",$STRUCTID$)
//! endtextmacro

//! textmacro AMHS_GetStructUnitGraphic takes UNITID, STRUCTID
set $STRUCTID$ = GetStoredInteger(fp_handlecache,I2S(H2I($UNITID$)),"1")
//! endtextmacro

//! textmacro AMHS_DestroyStructUnitGraphic takes UNITID
call StoreInteger(fp_handlecache,I2S(H2I($UNITID$)),"1",0)
//! endtextmacro

//! textmacro AMHS_StoreStructBuff takes UNITID, STRUCTID
call StoreInteger(fp_handlecache,I2S(H2I($UNITID$)),"2",$STRUCTID$)
//! endtextmacro

//! textmacro AMHS_GetStructBuff takes UNITID, STRUCTID
set $STRUCTID$ = GetStoredInteger(fp_handlecache,I2S(H2I($UNITID$)),"2")
//! endtextmacro

//! textmacro AMHS_DestroyStructBuff takes UNITID
call StoreInteger(fp_handlecache,I2S(H2I($UNITID$)),"2",0)
//! endtextmacro

//! textmacro AMHS_StructInitializeLine
call InitializeHandleCache()
//! endtextmacro
endlibrary
//===========================================================================
//function InitTrig_AMHS_Game_Cache takes nothing returns nothing
//    set gg_trg_AMHS_Game_Cache = CreateTrigger(  )
//endfunction