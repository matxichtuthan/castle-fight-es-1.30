library TileDefinition /* v1.2b    By IcemanBo
               
   */ requires /*

        */ WorldBounds    /* github.com/nestharus/JASS/blob/master/jass/Systems/WorldBounds/script.j  
               
**
**
**                          Information
**                         _____________
**
**  TileDefinition provides an API to give information about a terrain tile.
**            
**      
**                             API
**                           _______
**
**
**      function AreCoordinatesInSameTile takes real a, real b returns boolean
**          Checks if two coordinates share the same terrain tile.
**          
**          Attention: Only makes sense if both coordinates are of same type. Or x- or y coordinates.
**                     May bring wrong result, if you compare x with y coordinates.
**
**      function ArePointsInSameTile takes real x1, real y1, real x2, real y2 returns boolean
**          Checks if two points share the same terrain tile.
**
**
**      function GetTileCenterCoordinate takes real a returns real
**          Returns the cooridnate for the center of the tile.
**          Works for x- and y coordiantes.
**
**
**      function GetTileMax takes real a returns real
**          Returns the max value, that is still in same terrain tile.
**          Works for x- and y coordiantes.
**                  
**      function GetTileMin takes real a returns real
**          Returns the min value, that is still in same terrain tile.
**          Works for x- and y coordiantes.
**
**
**      funtion GetTileId takes real x, real y returns integer
**          Returns an unique index for tile of given coordinates.
**          Will return "-1" if it's invalid.
**
**      function GetTileCenterXById takes integer id returns real
**
**      function GetTileCenterYById takes integer id returns real
**                  
**
**      Credtis to Waterknight for some functions
**      
***********************************************************************************************************/
   
    /*
    *   TileDistance evaluates how many tiles the given
    *   cooridnate is away from reference tile. (located at 0/0)
    */
   
    private function TileDistance takes real a returns integer
        if (a >= 0) then
            return R2I((a + 64)/128)  
        endif
        return R2I((a - 64)/128)
    endfunction
   
    function GetTileMax takes real a returns real
        local integer aCount = TileDistance(a)
        if (aCount == 0) then
            return 64.
        elseif (aCount > 0) then
            return (aCount + 1.)*128. - 64.
        endif
        return (aCount)* 128. + 64.
    endfunction
   
    function GetTileMin takes real a returns real
        local integer aCount = TileDistance(a)
        if (aCount == 0) then
            return -64.
        elseif (aCount > 0) then
            return (aCount)*128. - 64.
        endif
        return (aCount - 1.)* 128. + 64.
    endfunction
   
    function AreCoordinatesInSameTile takes real a, real b returns boolean
        return (GetTileMin(a) == GetTileMin(b))
    endfunction
   
    function ArePointsInSameTile takes real x1, real y1, real x2, real y2 returns boolean
        return ((TileDistance(x1) == (TileDistance(x2)) and (TileDistance(y1) == (TileDistance(y2)))))
    endfunction
   
    function GetTileCenterCoordinate takes real a returns real
        if (a >= 0) then
            return GetTileMin(a) + 64.
        endif
        return GetTileMin(a) - 64.
    endfunction
   
    // Credits to Waterknight for following functions
   
    globals
        private integer WorldTiles_X
        private integer WorldTiles_Y
    endglobals
   
    function GetTileId takes real x, real y returns integer
        local integer xI = R2I(x - WorldBounds.minX + 64) / 128
        local integer yIs = R2I(y - WorldBounds.minY + 64) / 128

        if ((xI < 0) or (xI >= WorldTiles_X) or (yIs < 0) or (yIs >= WorldTiles_Y)) then
            return -1
        endif

        return (yIs * WorldTiles_X + xI)
    endfunction

    function GetTileCenterXById takes integer id returns real
        if ((id < 0) or (id >= WorldTiles_X * WorldTiles_Y)) then
            return 0.
        endif

        return (WorldBounds.minX + ModuloInteger(id, WorldTiles_X) * 128.)
    endfunction

    function GetTileCenterYById takes integer id returns real
        if ((id < 0) or (id >= WorldTiles_X * WorldTiles_Y)) then
            return 0.
        endif

        return (WorldBounds.minY + id / WorldTiles_X * 128.)
    endfunction
   
    private module Init
        private static method onInit takes nothing returns nothing
            set WorldTiles_X = R2I(WorldBounds.maxX - WorldBounds.minX) / 128 + 1
            set WorldTiles_Y = R2I(WorldBounds.maxY - WorldBounds.minY) / 128 + 1
        endmethod
    endmodule
   
    private struct TileDefinition extends array
        implement Init
    endstruct
   
endlibrary