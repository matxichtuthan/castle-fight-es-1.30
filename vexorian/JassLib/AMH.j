library AMH /* v1.0.0.3
*************************************************************************************
*
*   This will only stop maphack programs and will only work if the user attempts to
*   do anything to a unit. This will not stop the user from simply viewing the map
*   or units on the map.
*
*   This can be useful for keeping users from targeting units with spells (teleport etc)
*   as well as from controlling enemy units as well as from controlling their own units in
*   cases where their own units have no sight.
*
*   All of the SelectUnit stuff works fine with this, but the order stuff will not. If ordering
*   an active player unit to target something that's not visible to that player, this will
*   fire. However, non visible units can be ordered to do things.
*
*   This is a simple copy and paste.
*
*   This will notify other players in the game of the cheat as well as remove the cheating player from
*   the game.
*
************************************************************************************/
    globals
        private boolean s=false
        private timer e=CreateTimer()
    endglobals
    private function E takes nothing returns nothing
        set s=false
    endfunction
    private function S takes nothing returns boolean
        local player p=GetTriggerPlayer()
        if (not s and not IsUnitVisible(GetTriggerUnit(),p)) then
            call DisplayTimedTextToPlayer(GetLocalPlayer(),0,0,60,GetPlayerName(p) + " has cheated")
            if (p==GetLocalPlayer()) then
                call EndGame(false)
            endif
        else
            call TimerStart(e,0,false,function E)
        endif
        set p=null
        return false
    endfunction
    private function S2 takes nothing returns boolean
        local player p=GetTriggerPlayer()
        if (not s and GetOrderTargetUnit() != null and not IsUnitVisible(GetOrderTargetUnit(),p) and GetIssuedOrderId()!=851973) then
            call DisplayTimedTextToPlayer(GetLocalPlayer(),0,0,60,GetPlayerName(p) + " has cheated")
            if (p==GetLocalPlayer()) then
                call EndGame(false)
            endif
        endif
        set p=null
        return false
    endfunction
    private module I
        private static method onInit takes nothing returns nothing
            local integer i=11
            local trigger t=CreateTrigger()
            local trigger t2=CreateTrigger()
            local player p
            call TriggerAddCondition(t,Condition(function S))
            call TriggerAddCondition(t2,Condition(function S2))
            loop
                set p=Player(i)
                if (GetPlayerController(p)==MAP_CONTROL_USER and GetPlayerSlotState(p)==PLAYER_SLOT_STATE_PLAYING) then
                    call TriggerRegisterPlayerUnitEvent(t,p,EVENT_PLAYER_UNIT_SELECTED,null)
                    call TriggerRegisterPlayerUnitEvent(t2,p,EVENT_PLAYER_UNIT_ISSUED_TARGET_ORDER,null)
                endif
                exitwhen 0==i
                set i=i-1
            endloop
            set p=null
            set t=null
            set t2=null
        endmethod
    endmodule
    private struct N extends array
        implement I
    endstruct
    private function SUF takes unit u,boolean f returns nothing
        set s=true
    endfunction
    private function SU takes unit u returns nothing
        set s=true
    endfunction
    private function SUP takes unit u,player p returns nothing
        set s=true
    endfunction
    private function SG takes group g returns nothing
        set s=true
    endfunction
    private function SGP takes group g, player p returns nothing
        set s=true
    endfunction
    hook SelectUnit SUF
    hook SelectUnitAdd SU
    hook SelectUnitAddForPlayer SUP
    hook SelectUnitForPlayerSingle SUP
    hook SelectUnitSingle SU
    hook SelectGroupBJ SG
    hook SelectGroupForPlayerBJ SGP
endlibrary