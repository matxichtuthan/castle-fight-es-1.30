[Rehs]
Art=ReplaceableTextures\CommandButtons\BTNHardenedSkin.blp
Buttonpos=2,1
Hotkey=H
Name=Hardened Skin
Requires=etoe,eden
Tip=Research |cffffcc00H|rardened Skin
Ubertip=Gives Mountain Giants increased resistance to attack damage.

[Remg]
Art=ReplaceableTextures\CommandButtons\BTNUpgradeMoonGlaive.blp
Buttonpos=1,2
Hotkey=G
Name=Upgrade Moon Glaive
Requires=edob,etoe
Tip=Upgrade Moon |cffffcc00G|rlaive
Ubertip=Gives the Huntress the ability to strike additional units with her bouncing glaive attacks.

[Rguv]
Art=ReplaceableTextures\CommandButtons\BTNGlyph.blp
Name=Glyph of Ultravision

[Rhan]
Art=ReplaceableTextures\CommandButtons\BTNAnimalWarTraining.blp
Buttonpos=2,2
Hotkey=A
Name=Animal War Training
Requires=hlum,hcas,hbla
Tip=Research |cffffcc00A|rnimal War Training
Ubertip="Increases the maximum hit points of Knights, Dragonhawk Riders, and Gryphon Riders by 150."

[Rhar]
Art=ReplaceableTextures\CommandButtons\BTNHumanArmorUpOne.blp,ReplaceableTextures\CommandButtons\BTNHumanArmorUpTwo.blp,ReplaceableTextures\CommandButtons\BTNHumanArmorUpThree.blp
Buttonpos=,1
Hotkey=P
Name=Iron Plating,Steel Plating,Mithril Plating
Requires1=hkee
Requires2=hcas
Requirescount=3
Tip=Upgrade to Iron |cffffcc00P|rlating,Upgrade to Steel |cffffcc00P|rlating,Upgrade to Mithril |cffffcc00P|rlating
Ubertip="Increases the armor of Militia, Footmen, Spell Breakers, Knights, Flying Machines and Siege Engines.","Further increases the armor of Militia, Footmen, Spell Breakers, Knights, Flying Machines and Siege Engines."

[Rhlh]
Art=ReplaceableTextures\CommandButtons\BTNHumanLumberUpgrade1.blp,ReplaceableTextures\CommandButtons\BTNHumanLumberUpgrade2.blp
Hotkey=L
Name=Improved Lumber Harvesting,Advanced Lumber Harvesting
Requires=hkee
Requires1=hcas
Requirescount=2
Tip=Improved |cffffcc00L|rumber Harvesting,Advanced |cffffcc00L|rumber Harvesting
Ubertip=Increases the amount of lumber that Peasants can carry by 5.,Further increases the amount of lumber that Peasants can carry by 5.

[Rhme]
Art=ReplaceableTextures\CommandButtons\BTNSteelMelee.blp,ReplaceableTextures\CommandButtons\BTNThoriumMelee.blp,ReplaceableTextures\CommandButtons\BTNArcaniteMelee.blp
Hotkey=S
Name=Iron Forged Swords,Steel Forged Swords,Mithril Forged Swords
Requires1=hkee
Requires2=hcas
Requirescount=3
Tip=Upgrade to Iron Forged |cffffcc00S|rwords,Upgrade to Steel Forged |cffffcc00S|rwords,Upgrade to Mithril Forged |cffffcc00S|rwords
Ubertip="Increases the attack damage of Militia, Footmen, Spell Breakers, Dragonhawk Riders, Gryphon Riders and Knights.","Further increases the attack damage of Militia, Footmen, Spell Breakers, Dragonhawk Riders, Gryphon Riders and Knights."

[Rhpm]
Art=ReplaceableTextures\CommandButtons\BTNPackBeast.blp
Buttonpos=3
Hotkey=B
Name=Backpack
Requires=hvlt
Tip=Research |cffffcc00B|rackpack
Ubertip=Gives specific Human ground units the ability to carry items.
